<?php
require 'vendor/autoload.php';

// The multiplier for the amount of legos on your image, or "legolution" :)
$resolutionMultiplier = 2;

// When set to true it will only use lego colors that exists in real world.
$useLegoPalette = false;

$legofy = new \RicardoFiorani\Legofy\Legofy();

// $source can be any acceptable parameter for intervention/image
// Please see http://image.intervention.io/api/make
$source = 'https://clases-react.s3.eu-west-1.amazonaws.com/1624025332771.jpg';

/**
 *@var Intervention\Image\Image
 */
$output = $legofy->convertToLego($source, $resolutionMultiplier, $useLegoPalette);

// Please see http://image.intervention.io/use/basics and http://image.intervention.io/use/http
echo $output->response();